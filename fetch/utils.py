import torch
import numpy as np
from torch.distributions.categorical import Categorical
from torch.distributions.beta import Beta

# select actions
def select_actions(pis, exploration=True):
    if exploration:
        actions = []
        for pi in pis:
            action = Categorical(pi).sample().detach().cpu().numpy()
            action = np.expand_dims(action, 1)
            actions.append(action)
        actions = np.concatenate(actions, 1)
    else:
        # compute the mode of beta distributions
        actions = []
        for pi in pis:
            action = torch.argmax(pi, dim=1).item()
            actions.append(action)
        actions = np.array(actions)
    return actions

# get actions log prob and entropy
def evaluate_actions(pis, actions):
    log_prob = 0
    entropy = 0
    for i, pi in enumerate(pis):
        log_prob_temp = Categorical(pi).log_prob(actions[:, i])
        log_prob += log_prob_temp
        entropy_temp = Categorical(pi).entropy().mean()
        entropy += entropy_temp
    return log_prob.unsqueeze(1), entropy
