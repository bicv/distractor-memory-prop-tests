from arguments import get_args
from evaluate import fetch_evaluator
import numpy as np

if __name__ == '__main__':
    args = get_args()
    np.random.seed(123)
    # create the network
    tester = fetch_evaluator(args)
    tester.evaluate()
