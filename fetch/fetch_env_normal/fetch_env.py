import os
import numpy as np
import gym
import mujoco_py
from mujoco_py.modder import TextureModder
import copy
from gym import spaces
from fetch_env_normal.utils import robot_get_obs, ctrl_set_action, reset_mocap_welds, mocap_set_action
from gym.utils import seeding

class fetch_env:
    def __init__(self, height, width, tmax, domain_random, model_path, n_substeps, gripper_extra_height, block_gripper,\
                has_object, target_in_the_air, target_offset, obj_range, target_range, distance_threshold, initial_qpos, reward_type):
        fullpath = os.path.join(os.path.dirname(__file__), "assets", model_path)
        if not os.path.exists(fullpath):
            raise IOError('.xml file not exist!')
        # load the model
        model = mujoco_py.load_model_from_path(fullpath)
        self.sim = mujoco_py.MjSim(model, nsubsteps = n_substeps)
        self.modder = TextureModder(self.sim)
        # define the parameters
        self.height = height
        self.width = width
        self.gripper_extra_height = gripper_extra_height
        self.block_gripper = block_gripper
        self.has_object = has_object
        self.target_in_the_air = target_in_the_air
        self.target_offset = target_offset
        self.obj_range = obj_range
        self.target_range = target_range
        self.distance_threshold = distance_threshold
        self.reward_type = reward_type
        self.target_bounds = np.array([[1.195, 1.405], [0.595, 0.905]])
        self.tmax = tmax
        self.domain_random = domain_random
        # seed it
        self.seed()
        # setup the environment
        self._env_setup(initial_qpos=initial_qpos)
        self.initial_state = copy.deepcopy(self.sim.get_state())
        # sample goals
        self.goal = self._sample_goals()
        obs = self._get_obs()
        # get the space of the action
        self.action_space = spaces.Box(-1., 1., shape=(4,), dtype='float32')
        self.observation_space = None
        # get the control value
        self.control_values = (self.action_space.high - self.action_space.low) * 0.5

    # delta t, the time that do one action
    @property
    def dt(self):
        return self.sim.model.opt.timestep * self.sim.nsubsteps

    # step function
    def step(self, action):


        if self.domain_random:
            for name in self.sim.model.geom_names:
                if name == 'target':
                    pass
                else:
                    self.modder.rand_all(name)
            self.modder.rand_all('skybox')
        #action = np.clip(action, self.action_space.low, self.action_space.high)
        action = np.concatenate((action, [0]))
        new_control = np.zeros((4, ), dtype=np.float32)
        for action_idx in range(4):
            if action[action_idx] == 0:
                new_control[action_idx] = 0
            elif action[action_idx] == 1:
                new_control[action_idx] = self.control_values[action_idx] / 2
            elif action[action_idx] == 2:
                new_control[action_idx] = self.control_values[action_idx]
            elif action[action_idx] == 3:
                new_control[action_idx] = -self.control_values[action_idx] / 2
            elif action[action_idx] == 4:
                new_control[action_idx] = -self.control_values[action_idx]
        # set actions
        new_control = np.clip(new_control, self.action_space.low, self.action_space.high)
        self._set_action(new_control)
        self.sim.step()
        self._step_callback()
        obs = self._get_obs()
        done = self._is_success(obs['achieved_goal'], self.goal)
        if self.episode_timesteps >= self.tmax:
            done = True
        # compute the reward
        reward = self.compute_reward(obs['achieved_goal'], self.goal)
        self.episode_timesteps += 1
        return obs, reward, done

    # reset the environment
    def reset(self, fixed_pos=None):
        self.episode_timesteps = 0
        did_reset_sim = False
        while not did_reset_sim:
            did_reset_sim = self._reset_sim()
        # random the inital position
        """
        for _ in range(np.random.randint(20)):
            action = np.random.uniform(-1, 1, (4, ))
            self._set_action(action)
            self.sim.step()
            self._step_callback()
        """
        if fixed_pos is None:
            self.goal = self._sample_goals().copy()
        else:
            geom_positions = self.sim.model.geom_pos.copy()
            geom_positions[1] = fixed_pos
            self.sim.model.geom_pos[:] = geom_positions
            self.goal = fixed_pos
        self.sim.forward()



        # domain random
        if self.domain_random:
            for name in self.sim.model.geom_names:
                if name == 'target':
                    pass
                else:
                    self.modder.rand_all(name)
            self.modder.rand_all('skybox')
        obs = self._get_obs()
        return obs

    # function to calculate the rewards
    def compute_reward(self, achieved_goal, goal):
        d = self._goal_distance(achieved_goal, goal)
        if self.reward_type == 'sparse':
            return 1 - (d > self.distance_threshold).astype(np.float32)
        else:
            return -d

    # calculate the distance
    def _goal_distance(self, goal_a, goal_b):
        assert goal_a.shape == goal_b.shape
        return np.linalg.norm(goal_a - goal_b, axis=-1)

    def _is_success(self, achieved_goal, desired_goal):
        d = self._goal_distance(achieved_goal, desired_goal)
        if d <= self.distance_threshold:
            done = True
        else:
            done = False
        return done

    def _step_callback(self):
        if self.block_gripper:
            self.sim.data.set_joint_qpos('robot0:l_gripper_finger_joint', 0.)
            self.sim.data.set_joint_qpos('robot0:r_gripper_finger_joint', 0.)
            self.sim.forward()

    def _set_action(self, action):
        assert action.shape == (4,)
        action = action.copy()  # ensure that we don't change the action outside of this scope
        pos_ctrl, gripper_ctrl = action[:3], action[3]
        pos_ctrl *= 0.05  # limit maximum change in position
        rot_ctrl = [1., 0., 1., 0.]  # fixed rotation of the end effector, expressed as a quaternion
        gripper_ctrl = np.array([gripper_ctrl, gripper_ctrl])
        assert gripper_ctrl.shape == (2,)
        if self.block_gripper:
            gripper_ctrl = np.zeros_like(gripper_ctrl)
        action = np.concatenate([pos_ctrl, rot_ctrl, gripper_ctrl])
        # apply actions to simulation
        ctrl_set_action(self.sim, action)
        mocap_set_action(self.sim, action)

    # reset the simulation
    def _reset_sim(self):
        self.sim.set_state(self.initial_state)
        self.sim.forward()
        return True

    # function to get the observations
    def _get_obs(self):
        grip_pos = self.sim.data.get_site_xpos('robot0:grip')
        dt = self.sim.nsubsteps * self.sim.model.opt.timestep
        grip_velp = self.sim.data.get_site_xvelp('robot0:grip') * dt
        robot_qpos, robot_qvel = robot_get_obs(self.sim)
        if self.has_object:
            object_pos = self.sim.data.get_site_xpos('object0')
            # rotations
            object_rot = rotations.mat2euler(self.sim.data.get_site_xmat('object0'))
            # velocities
            object_velp = self.sim.data.get_site_xvelp('object0') * dt
            object_velr = self.sim.data.get_site_xvelr('object0') * dt
            object_rel_pos = object_pos - grip_pos
            object_velp -= grip_velp
        else:
            object_pos = object_rot = object_velp = object_velr = object_rel_pos = np.zeros(0)
        gripper_state = robot_qpos[-2:]
        gripper_vel = robot_qvel[-2:] * dt  # change to a scalar if the gripper is made symmetric
        if not self.has_object:
            achieved_goal = grip_pos.copy()
        else:
            achieved_goal = np.squeeze(object_pos.copy())
        # then get the rgb_view of the robotic
        rgb_obs = self.sim.render(width=self.width, height=self.height, camera_name='external_camera_0')
        #rgb_obs = self.sim.render(width=self.width, height=self.height, camera_name='head_camera_rgb')
        # concatenate the obeservation
        #obs = np.concatenate([grip_pos, gripper_state, grip_velp, gripper_vel])
        obs = np.concatenate([self.sim.data.qpos.flat[:], self.sim.data.qvel.flat[:]])
        return {
                'observation': obs.copy(),
                'achieved_goal': achieved_goal.copy(),
                'desired_goal': self.goal.copy(),
                'rgb_observation': rgb_obs.copy(),
                }

    def _env_setup(self, initial_qpos):
        for name, value in initial_qpos.items():
            self.sim.data.set_joint_qpos(name, value)
        reset_mocap_welds(self.sim)
        self.sim.forward()
        # move end effector into positions
        #gripper_target = np.array([-0.498, 0.005, -0.431 + self.gripper_extra_height]) + self.sim.data.get_site_xpos('robot0:grip')
        gripper_target = np.array([-0.498, 0.005, -0.35 + self.gripper_extra_height]) + self.sim.data.get_site_xpos('robot0:grip')
        gripper_rotation = np.array([1., 0., 1., 0.])
        self.sim.data.set_mocap_pos('robot0:mocap', gripper_target)
        self.sim.data.set_mocap_quat('robot0:mocap', gripper_rotation)
        for _ in range(10):
            self.sim.step()
        # extract information for sampling goals
        self.initial_gripper_xpos = self.sim.data.get_site_xpos('robot0:grip').copy()
        if self.has_object:
            self.height_offset = self.sim.data.get_site_xpos('object0')[2]

    # sample goals, position of balls
    def _sample_goals(self):
        goal = np.random.rand(2) * (self.target_bounds[:, 1] - self.target_bounds[:, 0]) + self.target_bounds[:, 0]
        goal = np.concatenate([goal, np.array([0.42])])
        geom_positions = self.sim.model.geom_pos.copy()
        prev_goal_pos = geom_positions[1]
        while (np.linalg.norm(prev_goal_pos - goal) < 0.1):
            goal = np.random.rand(2) * (self.target_bounds[:, 1] - self.target_bounds[:, 0]) + self.target_bounds[:, 0]
            goal = np.concatenate([goal, np.array([0.42])])
        geom_positions[1] = goal
        self.sim.model.geom_pos[:] = geom_positions
        return goal.copy()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]
