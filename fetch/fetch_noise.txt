fetch_noise.txt
the model path is: models/visual_std/seed_1/model.pt 
the md5 of model.pt is: 4e78bfc30dfb005f81a0acfb62efbc82 
seed: 0, the success ratio is: 0.9875 
the model path is: models/visual_std/seed_2/model.pt 
the md5 of model.pt is: bb41e34882f6403dae972a53d2630768 
seed: 1, the success ratio is: 0.9875 
the model path is: models/visual_std/seed_3/model.pt 
the md5 of model.pt is: e6b6e582b6e32ee22acfc60b10d3db43 
seed: 2, the success ratio is: 0.975 
the model path is: models/visual_std/seed_4/model.pt 
the md5 of model.pt is: 09d0d59e413af7fd50bb73da09eb4e7a 
seed: 3, the success ratio is: 0.9375 
the model path is: models/visual_std/seed_5/model.pt 
the md5 of model.pt is: 5ae62701424ee3fdf7c973ce72b6af58 
seed: 4, the success ratio is: 0.975 
mean: 0.9724999999999999, std: 0.01837117307087385, std_error: 0.008215838362577497 
----------------------------------------------------------- 
the model path is: models/sensor_std/seed_1/model.pt 
the md5 of model.pt is: 2485eafff4de978af32a18650b068241 
seed: 0, the success ratio is: 0.9875 
the model path is: models/sensor_std/seed_2/model.pt 
the md5 of model.pt is: 52ef45a1135a7a6acfdfa5f2d23ded01 
seed: 1, the success ratio is: 1.0 
the model path is: models/sensor_std/seed_3/model.pt 
the md5 of model.pt is: 95ee6c159921c30e52cd0a967c38cae1 
seed: 2, the success ratio is: 0.9375 
the model path is: models/sensor_std/seed_4/model.pt 
the md5 of model.pt is: d92a4333f9a01e9b8938265981cfe2d0 
seed: 3, the success ratio is: 1.0 
the model path is: models/sensor_std/seed_5/model.pt 
the md5 of model.pt is: 6c82bbfba0496410b45bc4ca74e11af9 
seed: 4, the success ratio is: 0.9875 
mean: 0.9824999999999999, std: 0.023184046238739264, std_error: 0.010368220676663861 
----------------------------------------------------------- 
the model path is: models/visual_random/seed_1/model.pt 
the md5 of model.pt is: 900a1bc25525e2d3d81cc3889e20324c 
seed: 0, the success ratio is: 0.975 
the model path is: models/visual_random/seed_2/model.pt 
the md5 of model.pt is: 2a32ce538d17427f21dc05be1b4aeea4 
seed: 1, the success ratio is: 0.95 
the model path is: models/visual_random/seed_3/model.pt 
the md5 of model.pt is: 66ae054e0c79f393ac7bdec33e05008d 
seed: 2, the success ratio is: 0.975 
the model path is: models/visual_random/seed_4/model.pt 
the md5 of model.pt is: 90f783a9c48277f203275cb3f9a7efa4 
seed: 3, the success ratio is: 0.9875 
the model path is: models/visual_random/seed_5/model.pt 
the md5 of model.pt is: fa7f9623a2a1877d77976997cdda7904 
seed: 4, the success ratio is: 0.975 
mean: 0.9724999999999999, std: 0.012247448713915915, std_error: 0.005477225575051672 
----------------------------------------------------------- 
the model path is: models/sensor_random/seed_1/model.pt 
the md5 of model.pt is: 339182d0e342e0f5ffe3fb89395d2fd6 
seed: 0, the success ratio is: 0.9875 
the model path is: models/sensor_random/seed_2/model.pt 
the md5 of model.pt is: 5aa7a350c8b5f7626cdf0fbdd51eabe0 
seed: 1, the success ratio is: 1.0 
the model path is: models/sensor_random/seed_3/model.pt 
the md5 of model.pt is: 44368ae3041a2aa9d756b867725e4a75 
seed: 2, the success ratio is: 0.975 
the model path is: models/sensor_random/seed_4/model.pt 
the md5 of model.pt is: bcf0f5af045f5b7f9c087f12f7f66291 
seed: 3, the success ratio is: 0.975 
the model path is: models/sensor_random/seed_5/model.pt 
the md5 of model.pt is: 158502ece09a6ad0bcd5a79e930a80f9 
seed: 4, the success ratio is: 0.975 
mean: 0.9824999999999999, std: 0.010000000000000014, std_error: 0.004472135954999586 
----------------------------------------------------------- 
