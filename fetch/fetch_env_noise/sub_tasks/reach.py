from fetch_env_noise import fetch_env

class reach_env(fetch_env.fetch_env):
    def __init__(self, height, width, tmax, domain_random, reward_type='sparse'):
        initial_qpos = {
                'robot0:slide0': 0.4049,
                'robot0:slide1': 0.48,
                'robot0:slide2': 0.0,
                }
        super(reach_env, self).__init__(height = height, width=width, tmax=tmax, domain_random=domain_random, \
                            model_path= 'fetch/reach_random.xml' if domain_random else 'fetch/reach.xml', has_object=False, 
                            block_gripper=True, n_substeps=75, gripper_extra_height=0.2, target_in_the_air=True, \
                            target_offset=0.0, obj_range=0.15, target_range=0.15, distance_threshold=0.05, \
                            initial_qpos=initial_qpos, reward_type=reward_type)
