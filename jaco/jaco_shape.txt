jaco_shape.txt
the model path is: models/visual_std/seed_1/model.pt 
the md5 of model.pt is: 30f4282ff725f046f8c48df545c711f2 
seed_0, the success ratio is: 0.04 
the model path is: models/visual_std/seed_2/model.pt 
the md5 of model.pt is: 6d6af17efab0922ef580ebffd77f3c94 
seed_1, the success ratio is: 0.0 
the model path is: models/visual_std/seed_3/model.pt 
the md5 of model.pt is: e2fd8a74bd010e87c086da59c8071791 
seed_2, the success ratio is: 0.0 
the model path is: models/visual_std/seed_4/model.pt 
the md5 of model.pt is: a7e74ed49f6139f74cab152c06da0189 
seed_3, the success ratio is: 0.0 
the model path is: models/visual_std/seed_5/model.pt 
the md5 of model.pt is: fc2b0e4d4fb7a2627f0085c99186e49d 
seed_4, the success ratio is: 0.0 
mean: 0.008, std: 0.016, std_error: 0.007155417527999326 
----------------------------------------------------------- 
the model path is: models/sensor_std/seed_1/model.pt 
the md5 of model.pt is: a1c7ba6dbcbdb3a49b10c046c86ed207 
seed_0, the success ratio is: 0.08 
the model path is: models/sensor_std/seed_2/model.pt 
the md5 of model.pt is: 477a8f0c61afba35ff4ae470794274a4 
seed_1, the success ratio is: 0.04 
the model path is: models/sensor_std/seed_3/model.pt 
the md5 of model.pt is: 2f755d8830ef283e682e71acb2b72b2e 
seed_2, the success ratio is: 0.02 
the model path is: models/sensor_std/seed_4/model.pt 
the md5 of model.pt is: 049bbf8dcc15fc6eb5818a6bf8248e70 
seed_3, the success ratio is: 0.06 
the model path is: models/sensor_std/seed_5/model.pt 
the md5 of model.pt is: f66309b7ed29c61d85b24aa35ab93eab 
seed_4, the success ratio is: 0.0 
mean: 0.039999999999999994, std: 0.0282842712474619, std_error: 0.012649110640673518 
----------------------------------------------------------- 
the model path is: models/visual_random/seed_1/model.pt 
the md5 of model.pt is: 8faab1560ed064509ad3e87f5d60f604 
seed_0, the success ratio is: 0.78 
the model path is: models/visual_random/seed_2/model.pt 
the md5 of model.pt is: 6cdcf427a03287bde71a715d834eaa9f 
seed_1, the success ratio is: 0.66 
the model path is: models/visual_random/seed_3/model.pt 
the md5 of model.pt is: f2ba20aef4d3d8690ee9e78b040e8196 
seed_2, the success ratio is: 0.66 
the model path is: models/visual_random/seed_4/model.pt 
the md5 of model.pt is: 59d933a98e26b733273bc60689f881ef 
seed_3, the success ratio is: 0.64 
the model path is: models/visual_random/seed_5/model.pt 
the md5 of model.pt is: 23e4b30864776d80c8f353ced1eb9b15 
seed_4, the success ratio is: 0.48 
mean: 0.644, std: 0.09583318840568752, std_error: 0.042857904755132396 
----------------------------------------------------------- 
the model path is: models/sensor_random/seed_1/model.pt 
the md5 of model.pt is: d47466bba99ecc8b7802e92cb90376ce 
seed_0, the success ratio is: 0.96 
the model path is: models/sensor_random/seed_2/model.pt 
the md5 of model.pt is: 96e76547708fc2f7c8d387189e83968b 
seed_1, the success ratio is: 0.94 
the model path is: models/sensor_random/seed_3/model.pt 
the md5 of model.pt is: 2521b1d3edcfc1e077f61143e930155b 
seed_2, the success ratio is: 1.0 
the model path is: models/sensor_random/seed_4/model.pt 
the md5 of model.pt is: 9fa25598e6c4628ca5c3a581bba38817 
seed_3, the success ratio is: 0.92 
the model path is: models/sensor_random/seed_5/model.pt 
the md5 of model.pt is: 0206ef93a8bc165604d2a5fdfdf11225 
seed_4, the success ratio is: 0.88 
mean: 0.9400000000000001, std: 0.039999999999999994, std_error: 0.017888543819998316 
----------------------------------------------------------- 
