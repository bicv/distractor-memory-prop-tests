import os
import time
import mujoco_py
import numpy as np
import torch

from gym.utils import seeding
from mujoco_py.modder import TextureModder


class JacoEnv():
    def __init__(self,
                 width,
                 height,
                 frame_skip,
                 rewarding_distance,
                 control_magnitude,
                 reward_continuous,
                 use_distance_finger,
                 max_steps,
                 domain_random):

        self.frame_skip = frame_skip
        self.width = width
        self.height = height
        self.max_steps = max_steps
        self.reward_continuous = reward_continuous
        self.use_distance_finger = use_distance_finger
        self.num_actions = 6
        self.non_rgb_state_size = 18
        self.counter = 0
        self.sum_position = torch.zeros(9)
        self.sum_velocity = torch.zeros(9)
        self.domain_random = domain_random
        # Instantiate Mujoco model
        if self.domain_random:
            model_path = "jaco_texture.xml"
        else:
            model_path = "jaco.xml"

        fullpath = os.path.join(
            os.path.dirname(__file__), "assets", model_path)
        if not os.path.exists(fullpath):
            raise IOError("File %s does not exist" % fullpath)
        model = mujoco_py.load_model_from_path(fullpath)
        self.sim = mujoco_py.MjSim(model)
        self.modder = TextureModder(self.sim)

        self.init_state = self.sim.get_state()
        self.init_qpos = self.sim.data.qpos.ravel().copy()
        self.init_qvel = self.sim.data.qvel.ravel().copy()

        # Setup actuators
        self.actuator_bounds = self.sim.model.actuator_ctrlrange
        self.actuator_low = self.actuator_bounds[:, 0]
        self.actuator_high = self.actuator_bounds[:, 1]
        self.actuator_ctrlrange = self.actuator_high - self.actuator_low
        self.num_actuators = len(self.actuator_low)

        # init model_data_ctrl
        self.null_action = np.zeros(self.num_actuators)
        self.sim.data.ctrl[:] = self.null_action

        self.seed()

        self.sum_reward = 0
        self.rewarding_distance = rewarding_distance

        # Target position bounds
        self.target_bounds = np.array(((0.2, 0.6), (0.1, 0.5), (0.05, 0.45)))
        self.target_reset_distance = 0.1

        # Setup discrete action space
        self.control_values = self.actuator_ctrlrange * control_magnitude

        self.num_actions = 5
        self.action_space = [list(range(self.num_actions))
                             ] * self.num_actuators
        self.observation_space = ((0, ), (height, width, 3),
                                  (height, width, 3))

        self._cache_dist = np.zeros(3)

        self.epoch_number = 0

        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def set_qpos_qvel(self, qpos, qvel):
        assert qpos.shape == (self.sim.model.nq, ) and qvel.shape == (
            self.sim.model.nv, )
        self.sim.data.qpos[:] = qpos
        self.sim.data.qvel[:] = qvel
        self.sim.forward()

    def reset(self, forced_target_position=None):
        #  Fixed initial position of Jaco
        # qpos = self.init_qpos
        qpos = np.array([0,-0.7,0,0,0,0,0,0,0])
        # Random initial position of Jaco
        #qpos += np.random.randn(qpos.size) * 0.25
        qvel = self.init_qvel

        # random object position start of episode
        if forced_target_position is not None:
            self.set_target_position(forced_target_position)
        else:
            self.reset_target()

        # set initial joint positions and velocities
        self.set_qpos_qvel(qpos, qvel)
        # clear the num steps...
        self.num_steps = 0
        self.epoch_number += 1
        return self.get_obs()

    def reset_target(self):
        # Randomize goal position within specified bounds
        self.goal = np.random.rand(3) * (self.target_bounds[:, 1] -
                                         self.target_bounds[:, 0]
                                         ) + self.target_bounds[:, 0]
        geom_positions = self.sim.model.geom_pos.copy()
        prev_goal_location = geom_positions[1]

        while (np.linalg.norm(prev_goal_location - self.goal) <
               self.target_reset_distance):
            self.goal = np.random.rand(3) * (self.target_bounds[:, 1] -
                                             self.target_bounds[:, 0]
                                             ) + self.target_bounds[:, 0]

        geom_positions[1] = self.goal
        self.sim.model.geom_pos[:] = geom_positions

    def set_target_position(self, position):
        self.goal = position
        geom_positions = self.sim.model.geom_pos.copy()
        geom_positions[1] = self.goal
        self.sim.model.geom_pos[:] = geom_positions

    def render(self, camera_name=None):
        rgb = self.sim.render(
            width=self.width, height=self.height, camera_name=camera_name)
        return rgb

    # getting joint observations meaning proprioception info--> joint angles and
    # velocities
    def _get_obs_joint(self):
        avg_pos =[-6.19734272e-05, -5.47909477e-05,  8.77889844e-05,  3.05766892e-05, 9.01806952e-05,
        -6.58938255e-05,  3.60653668e-06,  2.53475924e-06, 4.82505841e-06]

        avg_vel = [-1.26928233e-04,  2.00492889e-04,  2.00342663e-04,  1.97242535e-04, 1.98502850e-04,
        -2.00001351e-04,  6.21359351e-06,  6.65791858e-06, 1.34087841e-05]

        return np.concatenate(
            [avg_pos, self.sim.data.qvel.flat[:]])

        #self.counter += 1
        #self.sum_position += self.sim.data.qpos.flat[:]
        #self.sum_velocity += self.sim.data.qvel.flat[:]

        #if self.counter % 100 == 0:
            #print('Average positions are: '+ str(self.sim.data.qpos.flat[:]/self.counter))
            #print('Average velocities are: '+ str(self.sim.data.qvel.flat[:]/self.counter))
            #print('Counter is: ' + str(self.counter))
        #return np.concatenate(
            #[self.sim.data.qpos.flat[:], self.sim.data.qvel.flat[:]])


    # returning zeros for proprioception in order to learn the task solely by
    # here 18 refers to number of joints * 2 --> velocity and joint angles
    # def _get_obs_joint(self):
    #     return np.zeros(18)

    def _get_obs_rgb_view1(self):
        obs_rgb_view1 = self.render(camera_name='view1')
        # obs_rgb_view1 = np.zeros((64, 64, 3), dtype=np.uint8)
        return obs_rgb_view1

    def _get_obs_rgb_view2(self):
        # obs_rgb_view2 = np.zeros((64, 64, 3), dtype=np.uint8)
        obs_rgb_view2 = self.render(camera_name='view2')
        return obs_rgb_view2

    def get_obs(self):
        return (self._get_obs_joint(), self._get_obs_rgb_view1(),
                self._get_obs_rgb_view2())

    def do_simulation(self, ctrl):
        '''Do one step of simulation, taking new control as target

        Arguments:
            ctrl {np.array(num_actuator)}  -- new control to send to actuators
        '''
        ctrl = np.min((ctrl, self.actuator_high), axis=0)
        ctrl = np.max((ctrl, self.actuator_low), axis=0)

        self.sim.data.ctrl[:] = ctrl

        for _ in range(self.frame_skip):
            self.sim.step()

    # @profile(immediate=True)
    def step(self, a):
        done = False
        # domain randomnization
        if self.domain_random:
            for name in self.sim.model.geom_names:
                if name == "target":
                    pass
                else:
                    if self.epoch_number < 2:
                        self.modder.rand_all(name)

        new_control = np.copy(self.sim.data.ctrl).flatten()
        # Compute reward:
        reward = 0
        if self.use_distance_finger:
            # If any finger is close enough to target => +1
            self._cache_dist[0] = np.linalg.norm(
                self.sim.data.get_body_xpos("jaco_link_finger_1") - self.goal)
            self._cache_dist[1] = np.linalg.norm(
                self.sim.data.get_body_xpos("jaco_link_finger_2") - self.goal)
            self._cache_dist[2] = np.linalg.norm(
                self.sim.data.get_body_xpos("jaco_link_finger_3") - self.goal)
            if any(d < self.rewarding_distance for d in self._cache_dist):
                reward = 1
                self.reset_target()
        else:
            # If marker is on the target => +1
            dist = np.linalg.norm(self.sim.data.get_geom_xpos("marker") - self.goal)
            if dist < 0.1:
                reward = 1
                # self.reset_target()
                done = True

        if self.reward_continuous:
            reward = float((np.mean(dist)**-1)*0.1)

        if self.num_steps >= self.max_steps:
            done = True

        # Transform discrete actions to continuous controls
        for i in range(self.num_actuators):
        #    '''
        #    0 = 0 velocity
        #    1 = small positive velocity
        #    2 = large positive velocity
        #    3 = small negative velocity
        #    4 = large negative velocity
        #    '''
            if a[i] == 0:
                new_control[i] = 0
            if a[i] == 1:
                new_control[i] = self.control_values[i] / 2
            if a[i] == 2:
                new_control[i] = self.control_values[i]
            if a[i] == 3:
                new_control[i] = -self.control_values[i] / 2
            elif a[i] == 4:
                new_control[i] = -self.control_values[i]

        # Do one step of simulation
        self.do_simulation(new_control)
        self.sum_reward += reward
        self.num_steps += 1
        return self.get_obs(), reward, done
