jaco_no_ball.txt
the model path is: models/visual_std/seed_1/model.pt 
the md5 of model.pt is: 30f4282ff725f046f8c48df545c711f2 
seed_0, the success ratio is: 0.18 
the model path is: models/visual_std/seed_2/model.pt 
the md5 of model.pt is: 6d6af17efab0922ef580ebffd77f3c94 
seed_1, the success ratio is: 0.04 
the model path is: models/visual_std/seed_3/model.pt 
the md5 of model.pt is: e2fd8a74bd010e87c086da59c8071791 
seed_2, the success ratio is: 0.0 
the model path is: models/visual_std/seed_4/model.pt 
the md5 of model.pt is: a7e74ed49f6139f74cab152c06da0189 
seed_3, the success ratio is: 0.0 
the model path is: models/visual_std/seed_5/model.pt 
the md5 of model.pt is: fc2b0e4d4fb7a2627f0085c99186e49d 
seed_4, the success ratio is: 0.08 
mean: 0.06, std: 0.06693280212272604, std_error: 0.029933259094191526 
----------------------------------------------------------- 
the model path is: models/sensor_std/seed_1/model.pt 
the md5 of model.pt is: a1c7ba6dbcbdb3a49b10c046c86ed207 
seed_0, the success ratio is: 0.2 
the model path is: models/sensor_std/seed_2/model.pt 
the md5 of model.pt is: 477a8f0c61afba35ff4ae470794274a4 
seed_1, the success ratio is: 0.08 
the model path is: models/sensor_std/seed_3/model.pt 
the md5 of model.pt is: 2f755d8830ef283e682e71acb2b72b2e 
seed_2, the success ratio is: 0.04 
the model path is: models/sensor_std/seed_4/model.pt 
the md5 of model.pt is: 049bbf8dcc15fc6eb5818a6bf8248e70 
seed_3, the success ratio is: 0.1 
the model path is: models/sensor_std/seed_5/model.pt 
the md5 of model.pt is: f66309b7ed29c61d85b24aa35ab93eab 
seed_4, the success ratio is: 0.0 
mean: 0.084, std: 0.06740919818541087, std_error: 0.030146309890266836 
----------------------------------------------------------- 
the model path is: models/visual_random/seed_1/model.pt 
the md5 of model.pt is: 8faab1560ed064509ad3e87f5d60f604 
seed_0, the success ratio is: 0.4 
the model path is: models/visual_random/seed_2/model.pt 
the md5 of model.pt is: 6cdcf427a03287bde71a715d834eaa9f 
seed_1, the success ratio is: 0.12 
the model path is: models/visual_random/seed_3/model.pt 
the md5 of model.pt is: f2ba20aef4d3d8690ee9e78b040e8196 
seed_2, the success ratio is: 0.24 
the model path is: models/visual_random/seed_4/model.pt 
the md5 of model.pt is: 59d933a98e26b733273bc60689f881ef 
seed_3, the success ratio is: 0.48 
the model path is: models/visual_random/seed_5/model.pt 
the md5 of model.pt is: 23e4b30864776d80c8f353ced1eb9b15 
seed_4, the success ratio is: 0.32 
mean: 0.312, std: 0.12496399481450647, std_error: 0.05588559742903353 
----------------------------------------------------------- 
the model path is: models/sensor_random/seed_1/model.pt 
the md5 of model.pt is: d47466bba99ecc8b7802e92cb90376ce 
seed_0, the success ratio is: 0.08 
the model path is: models/sensor_random/seed_2/model.pt 
the md5 of model.pt is: 96e76547708fc2f7c8d387189e83968b 
seed_1, the success ratio is: 0.14 
the model path is: models/sensor_random/seed_3/model.pt 
the md5 of model.pt is: 2521b1d3edcfc1e077f61143e930155b 
seed_2, the success ratio is: 0.18 
the model path is: models/sensor_random/seed_4/model.pt 
the md5 of model.pt is: 9fa25598e6c4628ca5c3a581bba38817 
seed_3, the success ratio is: 0.18 
the model path is: models/sensor_random/seed_5/model.pt 
the md5 of model.pt is: 0206ef93a8bc165604d2a5fdfdf11225 
seed_4, the success ratio is: 0.16 
mean: 0.14800000000000002, std: 0.03709447398198281, std_error: 0.016589153082662175 
----------------------------------------------------------- 
