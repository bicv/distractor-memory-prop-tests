from arguments import get_args
from evaluate import jaco_evaluator
import numpy as np

# visualize the robotic
if __name__ == '__main__':
    np.random.seed(123)
    args = get_args()
    # create the network
    tester = jaco_evaluator(args)
    tester.evaluate()
