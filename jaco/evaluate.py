import torch
import numpy as np
from models import Network
from utils import select_actions
import cv2
import hashlib

class jaco_evaluator:
    def __init__(self, args):
        self.args = args
        self.args.episode_eval = 50
        # create the environment according to the selection
        if self.args.env_type == 'normal':
            from jaco_arm_normal import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'color':
            from jaco_arm_color import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'shape':
            from jaco_arm_shape import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'light':
            from jaco_arm_light import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'noise':
            from jaco_arm_noise import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'reflection':
            from jaco_arm_reflection import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'shift':
            from jaco_arm_shift import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'invisible':
            from jaco_arm_invisible import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)



        elif self.args.env_type == 'memory_long':
            from jaco_arm_memory_long import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'memory_short':
            from jaco_arm_memory_short import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)

        elif self.args.env_type == 'no_prop':
            from jaco_arm_no_prop import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'no_pos':
            from jaco_arm_no_pos import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        elif self.args.env_type == 'no_vel':
            from jaco_arm_no_vel import JacoEnv
            self.env = JacoEnv(self.args.width, self.args.height, self.args.frame_skip, self.args.rewarding_distance, \
                        self.args.control_magnitude, self.args.reward_continuous, self.args.use_distance_finger, \
                        self.args.t_max_eval, self.args.domain_random)
        # define some basic information of jaco arm
        self.non_rgb_state_size = 18
        self.num_actions = 5
        # set the fixed point
        if self.args.evaluate_fixed_targets:
            self.target_positions = []
            for x in np.linspace(self.env.target_bounds[0, 0]*1., self.env.target_bounds[0, 1]*1., 10):
                for y in np.linspace(self.env.target_bounds[1, 0]*1., self.env.target_bounds[1, 1]*1., 5):
                    for z in np.linspace(self.env.target_bounds[2, 0]*1., self.env.target_bounds[2, 1]*1., 5):
                        self.target_positions.append([x, y, z])
            # setup the target position
            self.target_positions = np.array(self.target_positions)
            self.args.episode_eval = 50
        else:
            self.target_positions = [None] * self.args.episode_eval

    def _file_as_bytes(self, file):
        with file:
            return file.read()

    # evaluate the newtork
    def evaluate(self, network_weight=None):
        # load the weights
        save_dir = 'models/'
        # adjust the sequence of the model extracted - for easy paper writing
        #model_types = ['visual_std', 'sensor_std', 'visual_random', 'sensor_random']

        model_types = ['sensor_std','sensor_random']

        seed_numbers = 5
        distractor_name = 'jaco_{}.txt'.format(self.args.env_type)
        distractor_file = open(distractor_name, 'w')
        distractor_file.write(distractor_name + '\n')
        for model_type in model_types:
            if model_type in ['sensor_std', 'sensor_random']:
                # start to create the network
                self.net = Network(self.non_rgb_state_size, self.num_actions, self.args.hidden_size, False, self.args.visual_only)
            else:
                self.net = Network(self.non_rgb_state_size, self.num_actions, self.args.hidden_size, False, True)
            # setup the container to store the success rate
            success_total = []
            for seed_id in range(seed_numbers):
                model_path = save_dir + model_type + '/' + 'seed_{}/model.pt'.format(seed_id+1)
                distractor_file.write('the model path is: {} \n'.format(model_path))
                distractor_file.write('the md5 of model.pt is: {} \n'.format(\
                    hashlib.md5(self._file_as_bytes(open(model_path, 'rb'))).hexdigest()))
                print(hashlib.md5(self._file_as_bytes(open(model_path, 'rb'))).hexdigest())
                self.net.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
                # start
                reward_sum = 0
                out = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'),fps=10,frameSize= (600,600))



                for ep_idx in range(self.args.episode_eval):
                    obs = self.env.reset(self.target_positions[ep_idx])
                    non_rgb_obs_tensor, rgb_obs_tensor = self._get_input_data(obs)
                    hx = torch.zeros((1, self.args.hidden_size), dtype=torch.float32)
                    cx = torch.zeros((1, self.args.hidden_size), dtype=torch.float32)
                    if self.args.cuda:
                        hx = hx.cuda()
                        cx = cx.cuda()
                    reward_total = 0
                    for _ in range(self.args.t_max_eval):
                        if self.args.render:
                            # get the view 1 of the camera
                            view1 = self.env.sim.render(width=600, height=600, camera_name='view1')
                            view1 = view1[:, :, (2, 1, 0)]
                            view1 = np.rot90(view1, 2)
                            # get the view 2 of the camera
                            view2 = self.env.sim.render(width=600, height=600, camera_name='view2')
                            view2 = view2[:, :, (2, 1, 0)]
                            view2 = np.rot90(view2, 2)
                            # concatenate two images
                            blank_part = np.ones((600, 20, 3), dtype=np.uint8) * 255
                            view_total = np.concatenate([view1, blank_part, view2], 1)
                            out.write(view1)
                            #cv2.imwrite("test_%d.jpg" % distractor_name, view2)
                            cv2.imwrite("test_no_ball.jpg", view2)
                            #print('image produced')


                            #cv2.imshow('demo', view_total)
                            #cv2.waitKey(100)
                        with torch.no_grad():
                            _, pi, (hx, cx) = self.net(non_rgb_obs_tensor, rgb_obs_tensor, (hx, cx), 1)
                        # select actions
                        actions = select_actions(pi, False)
                        cpu_actions = actions
                        cpu_bounded = cpu_actions.copy()
                        obs, reward, done = self.env.step(cpu_bounded)
                        reward_total += reward
                        if done:
                            break
                        non_rgb_obs_tensor, rgb_obs_tensor = self._get_input_data(obs)
                    #if network_weight is None:
                    #    print('the reward of this episode is: {}'.format(reward_total))
                    reward_sum += reward_total
                reward_average = reward_sum / self.args.episode_eval
                distractor_file.write('seed_{}, the success ratio is: {} \n'.format(seed_id, reward_average))
                print('Episodes are: ' + str(self.args.episode_eval))
                print('seed_{}, the success rate is: {}'.format(seed_id, reward_average))
                success_total.append(reward_average)
            distractor_file.write('mean: {}, std: {}, std_error: {} \n'.format(np.mean(success_total), np.std(success_total), \
                    np.std(success_total) / np.sqrt(5)))
            distractor_file.write('----------------------------------------------------------- \n')
            print('mean: {}, std: {}, std_error: {} \n'.format(np.mean(success_total), np.std(success_total), \
                    np.std(success_total) / np.sqrt(5)))
        distractor_file.close()
        return reward_average

    # the function to get the input data
    def _get_input_data(self, obs):
        non_rgb_obs, rgb_obs_1, rgb_obs_2 = obs

        # convert it into tensors
        rgb_obs = np.transpose(rgb_obs_2, (2, 0, 1))
        rgb_obs_tensor = torch.tensor(rgb_obs, dtype=torch.float32).unsqueeze(0)
        non_rgb_obs_tensor = torch.tensor(non_rgb_obs, dtype=torch.float32).unsqueeze(0)
        if self.args.cuda:
            rgb_obs_tensor = rgb_obs_tensor.cuda()
            non_rgb_obs_tensor = non_rgb_obs_tensor.cuda()

        return non_rgb_obs_tensor, rgb_obs_tensor
