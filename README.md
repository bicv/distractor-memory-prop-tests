# Distractor Tests for Jaco and Fetch
please follow the following instructions:
## Instructions
- create the environment using `.yml` file.
```bash
conda env create -f environment.yml 
```
- activate the virtual environment
```bash
conda activate robotic
```

- put models into the corresponding folders - remeber modify the folder's name to `models` - [models_link](https://imperialcollegelondon.box.com/s/bfvks0l53anl8lg6dsn5auwe46f8pkmx)

- run the test
```bash
python demo.py --evaluate-fixed-targets --env-type=<Distractor type>
```
